#!/usr/bin/env python3
#Written by TheMas3212
import sys

Normal = {}
Normal['button'] = ''
Normal['controllerBefore'] = '{'
Normal['controllerBetween'] = '|'
Normal['controllerAfter'] = '}'
Normal['frameBefore'] = '['
Normal['frameBetween'] = '\n'
Normal['frameAfter'] = ']'

Minimal = {}
Minimal['button'] = ''
Minimal['controllerBefore'] = ''
Minimal['controllerBetween'] = '|'
Minimal['controllerAfter'] = ''
Minimal['frameBefore'] = ':'
Minimal['frameBetween'] = '\n'
Minimal['frameAfter'] = ''

# Seperator Selection
Seperators = Normal

ButtonMap = {}
ButtonMap['A'] = 'A'
ButtonMap['B'] = 'B'
ButtonMap['X'] = 'X'
ButtonMap['Y'] = 'Y'
ButtonMap['Up'] = '^'
ButtonMap['Down'] = 'v'
ButtonMap['Left'] = '<'
ButtonMap['Right'] = '>'
ButtonMap['LeftBumper'] = 'L'
ButtonMap['RightBumper'] = 'R'
ButtonMap['Start'] = 'S'
ButtonMap['Select'] = 's'
ButtonMap['Extra1'] = '1'
ButtonMap['Extra2'] = '2'
ButtonMap['Extra3'] = '3'
ButtonMap['Extra4'] = '4'


def usage_exit():
    print('\nUsage: ' + sys.argv[0] + ' inputfile outputfile controllers')
    print('controllers should be a list of controller numbers 0-indexed')
    print('eg \'0145\' or \'01234567\'')
    sys.exit()

def read_controller_r16m(data):
    byteInt = int(data.hex(),16)
    inputdata = {
        ButtonMap['A']:             byteInt & 0x0080 != 0,
        ButtonMap['B']:             byteInt & 0x8000 != 0,
        ButtonMap['X']:             byteInt & 0x0040 != 0,
        ButtonMap['Y']:             byteInt & 0x4000 != 0,
        ButtonMap['Up']:            byteInt & 0x0800 != 0, 
        ButtonMap['Down']:          byteInt & 0x0400 != 0, 
        ButtonMap['Left']:          byteInt & 0x0200 != 0, 
        ButtonMap['Right']:         byteInt & 0x0100 != 0,
        ButtonMap['Start']:         byteInt & 0x1000 != 0,
        ButtonMap['Select']:        byteInt & 0x2000 != 0,
        ButtonMap['LeftBumper']:    byteInt & 0x0020 != 0,
        ButtonMap['RightBumper']:   byteInt & 0x0010 != 0,
        ButtonMap['Extra1']:        byteInt & 0x0008 != 0,
        ButtonMap['Extra2']:        byteInt & 0x0004 != 0,
        ButtonMap['Extra3']:        byteInt & 0x0002 != 0,
        ButtonMap['Extra4']:        byteInt & 0x0001 != 0
        }
    return inputdata

def read_r16m(file):
    with open(file, 'rb') as f:
        wholefile = f.read()
    bytesPerFrame = 16
    bytesPerController = 2
    numberOfControllers = int(bytesPerFrame/bytesPerController)
    totalFrames = int(len(wholefile)/bytesPerFrame)
    fulldata = []
    for frame in range(totalFrames):
        fulldata.append([None]*numberOfControllers)
        frameData = wholefile[frame*bytesPerFrame:(frame+1)*bytesPerFrame]
        for controller in range(numberOfControllers):
            controllerData = (frameData[controller*2:(controller+1)*2])
            fulldata[frame][controller] = read_controller_r16m(controllerData)
    return fulldata

def build_trf(inputData, controllers):
    trfData  = ''
    totalFrames = len(inputData)
    totalControllers = len(inputData[0])
    for frame in range(totalFrames):
        frameStr = str(frame) + Seperators['frameBefore']
        controllerList = []
        for controller in range(totalControllers):
            if str(controller) not in controllers:
                continue
            controllerStr = str(controller) + Seperators['controllerBefore']
            keyList = []
            for key, value in inputData[frame][controller].items():
                if value:
                    keyList.append(key)
            if len(keyList) > 0:
                controllerStr += Seperators['button'].join(keyList) + Seperators['controllerAfter']
                controllerList.append(controllerStr)
        if len(controllerList) > 0:
            frameStr += Seperators['controllerBetween'].join(controllerList)
            trfData += frameStr + Seperators['frameAfter'] + Seperators['frameBetween']
    return trfData

def main():
    try:
        if len(sys.argv) == 1:
            inputfile = input('Input Filename: ')
            outputfile = input('Output Filename: ')
            controllerSelect = input('Controllers: ')
        elif len(sys.argv) == 4:
            inputfile = sys.argv[1]
            outputfile = sys.argv[2]
            controllerSelect = sys.argv[3]
        else:
            usage_exit()
        if inputfile == '' or outputfile == '':
            usage_exit()
        print('Input Filename: ' + inputfile)
        print('Output Filename: ' + outputfile)
        print('Controllers: ' + controllerSelect)
        confirm = input('Confirm!? [y]')
        if confirm != 'y' :
            usage_exit()
    except KeyboardInterrupt:
        usage_exit()
    trfData = build_trf(read_r16m(inputfile), controllerSelect)
    with open(outputfile, 'w') as f:
        f.write(trfData)
    sys.exit()

if __name__ == '__main__':
    main()
